import { Injectable } from '@angular/core';
import { map, take } from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';
import {Contact} from './contact';

export interface Brand {
    name: string;
    get_realties?: Array<Realties>;
    brand: string;
    color?: string;
    disclaimer?: string;
    email?: string;
    key?: string;
    logo?: string;
    options?: string;
    validate?: string;
}

export interface Realties {
    name: string;
    get_offices?: Array<Office>;
    realty: string;
    IT?: string;
    billing_options?: string;
    brand?: string;
    color?: string;
    disclaimer?: string;
    email?: string;
    get_brand?: Brand;
    headquarter?: string;
    invoice?: string;
    key?: string;
    logo?: string;
    marketing?: string;
    options?: string;
    pricing?: string;
    realty_operations?: string;
    url?: string;
    validate?: string;
}

export interface Office {
    office: string;
    IT?: string;
    Invoice?: string;
    MLS?: string;
    Showcase?: string;
    address?: string;
    billing?: string;
    color?: string;
    email?: string;
    get_realty?: Realties;
    json_amount?: string;
    key?: string;
    logo?: string;
    manager?: string;
    office_operations?: string;
    pricing?: string;
    realty?: string;
    total_only?: string;
    url?: string;
    validate?: string;
}

@Injectable()
export class Agency {
    allBredes: Array<Brand>;
    brand: Brand = {name: '', brand: ''};
    realty: Realties = {name: '', realty: ''};
    office: Office = {office: ''};

    constructor(private http: HttpClient, public contact: Contact,
                ) {

    }

    getAgency(url) {
        if (!this.allBredes) {
            this.http.get(`${url}/api/v1/realty_options`).subscribe(
                data => this.success(data),
                err => this.error(err)
            );
        }
    }

    // alertBrand(){
    //     console.log(1, this.allBredes);
    //     let inputs = [];
    //     let brandes = this.allBredes;
    //
    //     for (let item in brandes){
    //         if( brandes.hasOwnProperty( item ) ){
    //             inputs.push({
    //                 name: brandes[item].name,
    //                 type: 'radio',
    //                 value: brandes[item],
    //                 label: brandes[item].name,
    //                 id: brandes[item].name
    //             });
    //         }
    //
    //     }
    //     let alert = this.alertCtrl.create({
    //         title: 'Brand',
    //         // message: "Enter a name for this new album you're so keen on adding",
    //         inputs: inputs,
    //         buttons: [
    //             {
    //                 text: 'Cancel',
    //                 handler: data => {
    //
    //                 }
    //             },
    //             {
    //                 text: 'Ok',
    //                 handler: data => {
    //                     if (data) {
    //                         console.log(data);
    //                         this.brand = data;
    //                         this.realty = {name: '', realty: ''};
    //                         this.office = {office: ''};
    //                         this.alertRealty(data);
    //                     } else {
    //                         return false;
    //                     }
    //                 }
    //             }
    //         ]
    //     });
    //     alert.present();
    // }
    //
    // alertRealty(data: Brand){
    //
    //     let inputs = [];
    //     let realty = data;
    //
    //     for (let item in realty.get_realties){
    //         if( realty.get_realties.hasOwnProperty( item ) ){
    //             inputs.push({
    //                 name: realty.get_realties[item].name,
    //                 type: 'radio',
    //                 value: realty.get_realties[item],
    //                 label: realty.get_realties[item].name,
    //                 id: realty.get_realties[item].name
    //             });
    //         }
    //
    //     }
    //     console.log(inputs);
    //
    //     if (inputs.length === 0) {
    //         this.cancel(data.name);
    //         return true;
    //     } else if (inputs.length === 1) {
    //         this.realty = inputs[0].value;
    //         this.alertOffice(inputs[0]);
    //         return true;
    //     } else {
    //         let alert = this.alertCtrl.create({
    //             title: 'Realty',
    //             // message: "Enter a name for this new album you're so keen on adding",
    //             inputs: inputs,
    //             buttons: [
    //                 {
    //                     text: 'Cancel',
    //                     handler: data => {
    //
    //                     }
    //                 },
    //                 {
    //                     text: 'Back',
    //                     handler: data => {
    //                         this.alertBrand();
    //                     }
    //                 },
    //                 {
    //                     text: 'Ok',
    //                     handler: data => {
    //                         console.log(data);
    //                         if (data) {
    //                             this.realty = data;
    //                             this.alertOffice(data);
    //                         } else {
    //                             return false
    //                         }
    //                     }
    //                 }
    //             ]
    //         });
    //         alert.present();
    //     }
    // }


    // alertOffice(data: Realties){
    //
    //     let inputs = [];
    //     let office = data;
    //
    //     for (let item in office.get_offices){
    //         if( office.get_offices.hasOwnProperty( item ) ){
    //             inputs.push({
    //                 name: office.get_offices[item].office,
    //                 type: 'radio',
    //                 value: office.get_offices[item],
    //                 label: office.get_offices[item].office,
    //                 id: office.get_offices[item].office
    //             });
    //         }
    //
    //     }
    //
    //     if (inputs.length === 0) {
    //         this.cancel(data.name);
    //         return true;
    //     } else if (inputs.length === 1) {
    //         this.office = inputs[0].value;
    //         this.cancel(inputs[0].value.office);
    //         return true;
    //     } else {
    //         let alert = this.alertCtrl.create({
    //             title: 'Office',
    //             // message: "Enter a name for this new album you're so keen on adding",
    //             inputs: inputs,
    //             buttons: [
    //                 {
    //                     text: 'Cancel',
    //                     handler: data => {
    //
    //                     }
    //                 },
    //                 {
    //                     text: 'Back',
    //                     handler: data => {
    //                         this.alertRealty(this.brand);
    //                     }
    //                 },
    //                 {
    //                     text: 'Ok',
    //                     handler: data => {
    //                         console.log(data);
    //                         if (data) {
    //                             this.office = data;
    //                             this.cancel(data.office);
    //                         } else {
    //                             return false;
    //                         }
    //                     }
    //                 }
    //             ]
    //         });
    //         alert.present();
    //     }
    //
    // }

    cancel(text) {
        this.contact.OfficeID = text;
        this.contact.office = this.office;
        this.contact.brand = this.brand;
        this.contact.realty = this.realty;
    }

    success(data) {
        this.allBredes = data;
        console.log(data);
    }

    error(err) {
        console.log(err);
    }
}