import { Injectable } from '@angular/core';
import { map, take } from 'rxjs/operators';
import {JwtHelperService } from '@auth0/angular-jwt';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Contact} from './contact';
import {Agency, Brand, Office, Realties} from './agency';
import { environment } from '../environments/environment';
import {RoomOnline} from '../models/roomOnline';
import * as io from "socket.io-client";

export class Role {
    id: string;
    name: string;
    display_name: string;
    description: string;
    created_at: string;
}


export interface Realties {
    name: string;
    get_offices?: Array<Office>;
    realty: string;
    IT?: string;
    billing_options?: string;
    brand?: string;
    color?: string;
    disclaimer?: string;
    email?: string;
    get_brand?: Brand;
    headquarter?: string;
    invoice?: string;
    key?: string;
    logo?: string;
    marketing?: string;
    options?: string;
    pricing?: string;
    realty_operations?: string;
    url?: string;
    validate?: string;
}

export class Agent {
    account: any;
    active: string;
    assistent: any;
    billing_vh: string;
    brand: string;
    get_office: Office;
    license: string;
    logo: string;
    notes: string;
    office: string;
    payerid: string;
    payment_profile: string;
    photo: string;
    photo_url: string;
    pricing: string;
    realty: string;
    self: string;
    showcase: string;
    url: string;
    video: string;
}

export class Profile {
    created_at?: any;
    email = '';
    first = '';
    key = '';
    last = '';
    phone = '';
    roles?: Array<Role>;
    updated_at?: string;
    validate?: string;
    agent?: Agent;
}

@Injectable()
export class Singleton {

    private dev = false;
    private prodServer = 'http://www.smartfloorplan.com/adminka/public';
    private devServer = 'http://adminka-laravel';
    private smartfloorplan_url = 'http://www.smartfloorplan.com';
    public serverURL: string = environment.url_adminka;
    private LOGIN_URL: string = this.serverURL + '/api/login';
    private LOGOUT_URL: string = this.serverURL + '/api/logout';
    public API_URL: string = this.serverURL + '/api/';
    private REFRESH_URL: string = this.serverURL + '/api/refresh';
    public SIGNUP_URL: string = this.serverURL + '/api/register';
    public UPDATE_URL: string = this.serverURL + '/api/update';
    public show_spinner = false;

    public  activeRoom:  any;

    socket: SocketIOClient.Socket;
    socketHost: string = environment.CHAT;
    room_online: Array<RoomOnline>;

    // When the page loads, we want the Login segment to be selected
    authType = 'login';

    // We need to set the content type for the server
    contentHeader = new HttpHeaders({'Content-Type': 'application/json'});
    error: string;

    readonly Agent = 'Agent';
    readonly Photographer: string = 'Photographer';
    user: string;
    token = '';
    profile: Profile = { email: '', first: '', key: '', last: '', phone: ''};
    jwtHelper = new JwtHelperService();

    data = 'init data';
    constructor(private http: HttpClient, public contact: Contact) {

        console.log('ready');
        try {
            if (localStorage.getItem('profile')) {
                this.profile = JSON.parse(localStorage.getItem('profile'));
            }
            if (localStorage.getItem('token')) {
                this.token = localStorage.getItem('token');
            }
        } catch (e) {
            console.log(e);
            this.token = undefined;
        }
        this.initContact();
        if (this.token !== null || this.token !== undefined || this.token !== 'undefined') {
            console.log('token ' + this.token);
            this.refreshToken();
        }

        //todo: logout login change
        this.initSocket();
    }

    public initSocket() {
        this.room_online = [];
        this.socket = io.connect(this.socketHost);
    }

    public getNewSocket() {
        return io.connect(this.socketHost);
    }

    public initContactToken() {
        this.contact.token = this.token;
    }

    public initContact() {
        if (this.profile) {
            this.contact.first = this.profile.first;
            this.contact.last = this.profile.last;
            this.contact.phone = this.profile.phone;
            this.contact.email = this.profile.email;
            if (this.profile.agent) {
                this.contact.photo_url = this.profile.agent.photo_url;
                this.contact.office = this.profile.agent.get_office;
                if (this.contact.office) {
                    this.contact.realty = this.profile.agent.get_office.get_realty;
                    if (this.contact.realty) {
                        this.contact.brand = this.profile.agent.get_office.get_realty.get_brand;
                    } else {
                        this.contact.brand = {name: '', brand: ''};
                    }
                } else {
                    this.contact.realty = {name: '', realty: ''};
                    this.contact.brand = {name: '', brand: ''};
                }

                if (this.contact.brand) {
                    this.contact.OfficeID = this.contact.brand.name;
                }
                if (this.contact.realty) {
                    this.contact.OfficeID = this.contact.realty.name;
                }
                if (this.contact.office) {
                    this.contact.OfficeID = this.contact.office.office;
                }
            }
            this.contact.token = this.token;


        } else {
            this.contact.first = '';
            this.contact.last = '';
            this.contact.phone = '';
            this.contact.email = '';
        }
    }

    public updateProfile(profile: Profile) {
        this.profile = profile;
        localStorage.setItem('profile', JSON.stringify(profile));
    }

    refreshToken() {
        this.http.post(this.REFRESH_URL + '?token=' + this.token, { headers: this.contentHeader })
            .subscribe(
                res => this.refreshTokenSucceses(res),
                (err) => this.authError(err)
            );
    }

    refreshTokenSucceses(data) {
        if (data.error) {
            console.log(data);
        } else {
            localStorage.setItem('token', data.token);
            this.token = data.token;
            this.initContactToken();
        }
    }

    hasRole(role) {
        // console.log("role ", role );

        let reg;
           reg = new RegExp(role);
        function findRole(element, index, array) {
            return reg.test(element.name);
        }
        if (this.profile && typeof this.profile.roles === 'object') {
            return this.profile.roles.findIndex(findRole) !== -1;
        } else {
            return false;
        }

    }


    spinner_show() {
        this.show_spinner = true;
    }

    spinner_hide() {
        this.show_spinner = false;
    }

    authenticated(token) {
        let result: boolean;
            result = false;
        if (token === undefined || token === 'undefined' || token === '') {
            return result;
        }
        let decode;
            decode = this.jwtHelper.decodeToken(token);
        // console.log(decode);
        // console.log(this.profile);
        if (decode) {
            result = decode.exp * 1000 > Date.now();
        }
        return result;
    }

    set(data) {
        this.data = data;
    }

    log() {
        console.log(this.data);
    }


    login(credentials) {
        this.spinner_show();
            this.http.post(this.LOGIN_URL, JSON.stringify(credentials), { headers: this.contentHeader })
                .subscribe(
                    res => this.authSuccess(res, credentials),
                    (err) => this.authError(err)
                );
    }

    signup(credentials) {
        this.http.post(this.SIGNUP_URL, JSON.stringify(credentials), {headers: this.contentHeader})
            .subscribe(
                data => this.authSuccess(data, credentials),
                err => this.authError(err)
            );
    }

    authError(err) {
        this.spinner_hide();
        this.error = err + ' ' + this.LOGIN_URL;
    }

    authSuccess(token, credentials) {
        this.spinner_hide();
        if (token.error) {
            return this.error = 'Error: ' + token.result;
        }
        this.error = null;

        this.token = token.token;
        this.profile = token.profile;
        this.initContact();

        localStorage.setItem('token', token.token);
        localStorage.setItem('profile', JSON.stringify(token.profile));
        localStorage.setItem('role', token.role);

    }

    authenticate(credentials) {
        this.authType === 'login' ? this.login(credentials) : this.signup(credentials);
    }

    logout() {
        localStorage.removeItem('token');
        localStorage.removeItem('role');
        localStorage.removeItem('profile');
        this.token = null;
        this.user = null;

                this.http.post(this.LOGOUT_URL, {headers: this.contentHeader})
                    .subscribe(
                        data => {},
                        err => {}
                    );

    }
}