import { Injectable } from '@angular/core';
import { map, take } from 'rxjs/operators';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Singleton} from './singleton';
import {Agency, Brand, Office, Realties} from './agency';
// import moment from 'moment';


@Injectable()
export class Contact {
    public first = '';
    public last = '';
    public email = '';
    public phone = '';
    public password?: string;
    public confirm_password?: string;
    public OfficeID: string;
    public office: Office;
    public brand: Brand;
    public realty: Realties;
    public photo_url?: string;
    public token?: string;
    public formErrors: Object = {};
    // public timeStart?: string = moment().startOf('day').format('HH:mm');
    // public timeEnd?: string = moment().endOf('day').format('HH:mm');


    constructor(private http: HttpClient) {

    }
    toJSON() {
        return {
            first: this.first,
            last: this.last,
            email: this.email,
            phone: this.phone,
            password: this.password,
            password_confirmation: this.confirm_password,
            OfficeID: this.OfficeID,
            office: this.office ? this.office.office : '',
            brand: this.brand ? this.brand.name : '',
            realty: this.realty ? this.realty.name : '',
            photo_url: this.photo_url
        };
    }
    create(single: Singleton) {
        let data_contact = JSON.stringify(this);
        this.http.post( single.SIGNUP_URL, data_contact, {headers: single.contentHeader })
            .subscribe(
                data => this.authSuccess(data, single),
                err => this.authError(err)
            );
        console.log(data_contact);
    }
    update(single: Singleton) {
        let data_contact = JSON.stringify(this);
        console.log(data_contact, this);

        this.http.post( single.UPDATE_URL + '?token=' + this.token, data_contact, {headers: single.contentHeader })
            .subscribe(
                data => this.authUpdate(data, single),
                err => this.authError(err)
            );
        console.log(data_contact);
    }
    authUpdate(data, single: Singleton){
        console.log(data);

        single.updateProfile(data);
    }

    authSuccess(data, single: Singleton){
        console.log(data);
        single.authenticate({email : this.email, password: this.password});
    }
    authError(err) {
        if(err.status === 422) {
            this.formErrors = err.error;
        }
        console.error(err, this.formErrors);
    }

    setFirst(first) {
        this.first = first;
    }
    setLast(last) {
        this.last = last;
    }
    setEmail(email) {
        this.email = email;
    }
    setPhone(phone) {
        this.phone = phone;
    }
    setPassword(password) {
        this.password = password;
    }

    setConfirmPassword(confirm_password){
        this.confirm_password = confirm_password;
    }
}