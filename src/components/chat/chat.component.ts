import {Component, EventEmitter, Input, Output} from '@angular/core';
import { Product } from '../../models/product';
import {Room} from '../../models/room';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Singleton} from '../../provider/singleton';
import {RoomOnline} from '../../models/roomOnline';


export interface Message {
    message?: string;
    username?: string;
    user_name?: string;
    show?: boolean;
    index?: number;
    text?: string;
}

export interface PresetStrings {
    updata_at: string;
    presets: any;
}

@Component({
    selector: 'app-chat',
    templateUrl: 'chat.component.html',
    styleUrls: ['./chat.component.css']
})

export class ChatComponent {
    @Input() allRoom: Array<Room>;
    @Input() room_online: Array<RoomOnline>;
    @Output() allRoomChange = new EventEmitter <Array<Room>>() ;
    @Input() activeRoom: Room;

    public products: Product[] = [
        new Product(1, 'Product 001'),
    ];


    API: string = this.single.serverURL + '/api/';
    presetsStrings: PresetStrings = {updata_at: Date(), presets: []};
    messages: Array<Message> = [];
    username = '';
    chat: any;
    myMessage: boolean;
    opMessage: boolean;
    connected: boolean;
    closed = '';
    online = '';
    status = 'online';
    historyMessage: boolean;

    socket: SocketIOClient.Socket;

    constructor(private http: HttpClient, public single: Singleton) {
        this.username = this.single.profile.first + this.single.profile.last;
        this.myMessage = false;
        this.opMessage = false;
        this.connected = false;
        this.historyMessage = false;

        this.socket = single.getNewSocket();
        this.socket.emit('room agent', this.single.activeRoom.id);

        this.socket.on('chat message', (msg: Message) => {
            msg.show = this.showName(this.username === msg.username);
            msg.index =  this.messages.length;
            this.messages.push(msg);
            console.log(msg, this.messages);
            // if(this.content._scroll){
            //     this.content.scrollToBottom();
            // }

        }).on('connect', () => {
            this.connected = true;
            // Connected, let's sign-up for to receive messages for this room
        }).on('login', (data) => {
            this.connected = true;
        }).on('room history message', (data) => {
            console.log(data);
            // if (this.historyMessage) { return; }
            // this.historyMessage = true;
            for ( let i = 0, l = data.length; i < l; i++ ) {
                let temp_val;
                this.messages = [];
                for ( const val of data) {

                    temp_val = val;


                    temp_val.message = val.text;

                    temp_val.username = val.user_name;
                    temp_val.show = this.showName(val.username === this.username);
                    temp_val.index =  this.messages.length;
                    this.messages.push(temp_val);
                }
            }
        }).on('customer closed chat mob', (data) => {
            console.log(data);
            this.closed = data;
        }).on('connect mob', (data) => {
            this.online = data;
            console.log(data);
        });
        this.getPreset();
    }

    getPreset() {
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.single.token
        });
        this.http.get(`${this.API}preset_string_chat?tokent=${this.single.token}}`, {
            headers: headers
        }).subscribe(
            data => this.successPreset(data),
            err => this.errorPreset(err)
        );
    }

    successPreset(data) {

        console.log(this.messages);

        // if (this.messages.length === 0 || (this.messages.length === 1 && this.messages[0].text === '')) {
        //     this.chatSend({chatText: data[0]});
        // }
        //
        // this.presetsStrings = {
        //     updata_at: Date(),
        //     presets: data.slice(1)
        // };

        this.presetsStrings = {
            updata_at: Date(),
            presets: data
        };
        console.log(data);
    }

    errorPreset(err) {
        console.log(err);
    }

    chatSend(v, x?) {
        const data = {
            message: v.chatText,
            username: this.username,
            show: this.showName(true)
        };
        v.chatText = '';
        this.socket.emit('chat message', data);
        this.chat = '';

    }

    showName(my: boolean) {
        if (my === true) {
            if (this.myMessage === true) {
                return false;
            } else {
                this.myMessage = true;
                this.opMessage = false;
                return true;
            }
        } else {
            if (this.opMessage === true) {
                return false;
            } else {
                this.opMessage = true;
                this.myMessage = false;
                return true;
            }
        }
    }

    chatColor() {
        let self;
        self = this;
        function findRoomInOnlineArray(element: RoomOnline) {
            return element.job_id === self.activeRoom.job_id && element.room_id.indexOf(self.activeRoom.room_id) !== -1;
        }
        let online: RoomOnline|undefined;
        online = this.room_online.find(findRoomInOnlineArray);

        if (this.closed === 'closed') {
            this.status = this.closed;
            return '#EF6C00';
        } else if (online !== undefined) {
            this.status = this.closed ? this.closed : 'online';
            return '#43A047';
        } else {
            this.status = 'offline';
            return '#C62828';
        }
    }

    deleteRoom() {
        console.log(this.allRoom);

        let self;
        self = this;

        function findRoom(element: Room) {
            return element.room_id === self.activeRoom.room_id;
        }

        let i;
        i = this.allRoom.findIndex(findRoom);


        if (i !== -1) {
            this.allRoom.splice(i, 1);
        }

        console.log(this.allRoom, i);
        this.allRoomChange.emit(this.allRoom);
        console.log(this.allRoom, i);
    }
}