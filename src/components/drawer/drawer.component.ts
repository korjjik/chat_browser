import {Component, EventEmitter, Input, Output} from '@angular/core';
import {RoomOnline} from '../../models/roomOnline';
import {Room} from '../../models/room';
import {Singleton} from '../../provider/singleton';
import {HttpClient} from '@angular/common/http';
@Component({
    selector: 'app-drawer',
    templateUrl: 'drawer.component.html',
    styleUrls: ['drawer.component.css']
})

export class DrawerComponent {
    @Input() room_online: Array<RoomOnline>;
    @Input() room_online_personal: Array<RoomOnline>;
    @Input() allRoom: Array<Room>;
    @Input() activeRoom: Room;
    @Output() activeRoomChange = new EventEmitter <Room>() ;
    socket: SocketIOClient.Socket;

    constructor(public single: Singleton) {
        this.socket = single.socket;
    }

    setActive(room: string, job_id: string) {

        console.log(room, job_id);
        let r: Room|undefined;
        r = this.allRoom.find(findRoom);
        console.log(r);
        if (r === undefined) {
            r = new Room(room, job_id);
            console.log(r);
            this.allRoom.push(r);
        }

        this.activeRoom = r;
        this.activeRoomChange.emit(r);

        console.log(this.allRoom, this.activeRoom);
        function findRoom(element: Room) {
            return element.job_id === job_id && element.room_id === room;
        }

        console.log('room agent', this.activeRoom.id);

        this.single.activeRoom = this.activeRoom;
    }
}