export class RoomOnline {
    job_id: string;
    room_id: Array<string>;

    constructor(job_id: string, room_ids: Object) {
        this.job_id = job_id;
        this.room_id = [];
        for (const room_id in room_ids){
            if ( room_ids.hasOwnProperty(room_id) ) {
                this.room_id.push(room_id);
            }
        }
    }
}