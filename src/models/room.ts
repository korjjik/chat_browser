export class Room {
    id: string;
    room_id: string;
    job_id: string;
    constructor (room_id: string, job_id: string) {
        this.room_id = room_id;
        this.job_id = job_id;
        this.id = job_id + room_id;
    }
}