import { RouterModule, Routes } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { ChatComponent } from '../components/chat/chat.component';

const routes: Routes = [
    // {
    //     path: '',
    //     component: ChatComponent,
    //     outlet: 'chat'
    // }
];

export const routingModule: ModuleWithProviders = RouterModule.forRoot(routes);