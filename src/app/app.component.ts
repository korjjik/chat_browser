import { Component } from '@angular/core';
import { environment } from '../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Singleton} from '../provider/singleton';
import {FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material';
import {RoomOnline} from '../models/roomOnline';
import {Room} from '../models/room';

export interface ItemJobs {
    address?: string;
    jobid?: string;
    status?: string;
}

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
    isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
        const isSubmitted = form && form.submitted;
        return (control && control.invalid && (control.dirty || control.touched || isSubmitted));
    }
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})

export class AppComponent {
    showFiller = false;
    // chatAll: any;
    viewAll: true;
    url: string = environment.url_adminka;
    API: string = this.single.serverURL + '/api/';
    error: any;
    items: Array<ItemJobs> = [];
    itemsAll: Array<ItemJobs>;
    matcher = new MyErrorStateMatcher();
    room_online: Array<RoomOnline> = [];
    room_online_personal: Array<RoomOnline> = [];
    activeRoom: Room = new Room('', '');
    allRoom: Array<Room> = [];
    show_spinner = false;
    socket: SocketIOClient.Socket;

    form = {
        email: '',
        password: ''
    };

    emailFormControl = new FormControl('', [
        Validators.required,
        Validators.email,
    ]);

    constructor(private http: HttpClient, public single: Singleton) {
        this.socket = single.socket;

        // this.socket.on('list chats agent', (data) => {
        //         this.chatAll = data;
        //         console.log('list chats agent', data);
        //     }
        // );

        this.socket.on('room online', (data) => {
                this.room_online = [];
                for ( const i in data) {
                    if ( data.hasOwnProperty(i) ) {
                        this.room_online.push(new RoomOnline(i, data[i]));
                    }
                }

                console.log('room online', data, this.room_online);
            }
        );


        this.socket.on('room online personal', (data) => {
                this.room_online_personal = [];
                for ( const i in data) {
                    if ( data.hasOwnProperty(i) ) {
                        this.room_online_personal.push(new RoomOnline(i, data[i]));
                    }
                }

                console.log('room online personal', data, this.room_online_personal);
            }
        );


        this.socket.emit('list chats', this.single.profile.email);

        this.socket.on('room add', () => {
                this.socket.emit('list chats', this.single.profile.email);
            }
        );
        this.socket.on('room remove', () => {
                this.socket.emit('list chats', this.single.profile.email);
            }
        );

        this.getPreset();
    }

    onSubmit(f: NgForm) {
        console.log(f.value);  // { first: '', last: '' }
        console.log(f.valid);  // false
        if (f.valid) {
            // console.log(window.OneSignal);
            // window.OneSignal.getUserId().then(function(userId) {
            //     console.log("OneSignal User ID:", userId);
            //     // (Output) OneSignal User ID: 270a35cd-4dda-4b3f-b04e-41d7463a2316
            // });

            this.single.login(f.value);
        }
    }

    getReceived() {
        if (this.viewAll) {
            return '';
// return "&received="+moment(this.event.timeEnds).format("MM/DD/YYYY") + " - " + moment(this.event.timeStarts).format("MM/DD/YYYY");
        } else {
            return '';
        }
    }

    filterItems() {
        let headers;
         headers = new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.single.token
        });
         this.single.spinner_show();
        this.http.get(`${this.API}agent-orders-data?token=${this.single.token}&limit=5000&received=${this.getReceived()}`, {
            headers: headers
        }).subscribe(
            data => this.successListJobs(data, this.single.profile),
            err => this.errorListJobs(err)
        );

    }

    successListJobs(data, profile) {
        this.single.spinner_hide();

        this.items = data.jobs;
        this.itemsAll = data.rows;

        console.log('socket ', this.socket);
        this.socket.emit('list chats', profile.email);
    }

    errorListJobs(err) {
        this.single.spinner_hide();
        this.error = err;
    }

    widthChat() {
        return Math.trunc(100 / this.allRoom.length ) + '%';
    }

    getPreset() {
        let headers;
        headers = new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.single.token
        });
        this.http.get(`${this.API}preset_string_chat?tokent=${this.single.token}}`, {
            headers: headers
        }).subscribe (
            data => this.successPreset(data),
            err => this.errorPreset(err)
        );
    }

    successPreset(data) {

        // console.log(this.messages);
        //
        // if(this.messages.length === 0 || (this.messages.length === 1 && this.messages[0].text === "")){
        //     this.chatSend({chatText:data[0]})
        // }
        //
        // this.presetsStrings = {
        //     updata_at: Date(),
        //     presets: data.slice(1)
        // }
        console.log('preset ', data);
    }

    errorPreset(err) {
        console.log(err);
    }
}
